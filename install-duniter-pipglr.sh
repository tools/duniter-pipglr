#!/bin/bash
set -ueo pipefail

# Usage: PIPGLR_TOKEN='<gitlab-runner-token>' ./install-duniter-pipglr.sh
#
# All environment variables below may be overridden
PIPGLR_USER="${PIPGLR_USER:-duniter-pipglr}"
PIPGLR_USER_HOME="${PIPGLR_USER_HOME:-/home/$PIPGLR_USER}"
PIPGLR_IMAGE="${PIPGLR_IMAGE:-localhost/duniter-pipglr:latest}"
PIPGLR_TOKEN="${PIPGLR_TOKEN}"

error () {
  retcode="$1"
  shift
  while [ $# -gt 0 ]; do
    echo "$1" >&2
    shift
  done
  exit "$retcode"
}

# User $PIPGLR_USER must not exist
! id "$PIPGLR_USER" &>/dev/null || error 1 \
  "Warning: this script is not idempotent." \
  "Remove user '$PIPGLR_USER' and its home directory before re-running:" \
  "  $ sudo loginctl disable-linger $PIPGLR_USER" \
  "  $ sudo deluser --remove-home $PIPGLR_USER"

# podman must be installed
podman --version || error 2 \
  "Cannot find 'podman'." \
  ". Please install it then re-run."

# Build the image
podman build --tag "$PIPGLR_IMAGE" -f Containerfile .

# Create user "$PIPGLR_USER"
sudo adduser --quiet --disabled-password --home "$PIPGLR_USER_HOME" --gecos "Gitlab runner for duniter project" "$PIPGLR_USER"

# Set /etc/sub{u,g}id
for type in subuid subgid; do
  # Delete existing entries for $PIPGLR_USER
  sudo sed -i "/^$PIPGLR_USER:/d" /etc/$type
  # Compute max allocated id
  first=$(sed -E 's/^.*:(.*):(.*)$/\1+\2/' /etc/$type | bc | sort -n | tail -1)
  # Allocate 65536+4 ids
  # See https://gitlab.com/qontainers/pipglr/-/issues/3
  sudo sh -c "echo '$PIPGLR_USER:$first:65540' >>/etc/$type"
done

# Enable linger
sudo loginctl enable-linger "$PIPGLR_USER"
sleep 5

sudo su - "$PIPGLR_USER" -c "
  export XDG_RUNTIME_DIR=/run/user/\$(id -u)
  systemctl --user stop podman.service
  systemctl --user stop podman.socket
  systemctl --user disable podman.service
  systemctl --user disable podman.socket
"

# Transfer the image to user "$PIPGLR_USER"
podman save "$PIPGLR_IMAGE" | sudo su - "$PIPGLR_USER" -c "podman load"

# Confgure the service
sudo su - "$PIPGLR_USER" -c "
  set -ueo pipefail
  export XDG_RUNTIME_DIR=/run/user/\$(id -u)
  PIPGLR_IMAGE=$PIPGLR_IMAGE
  echo '$PIPGLR_TOKEN' | podman secret create REGISTRATION_TOKEN -
  touch config.toml
  podman container runlabel register \$PIPGLR_IMAGE
  podman secret rm REGISTRATION_TOKEN
  sed -i -E 's:^( *volumes = ).*$:\1[\"/cache\", \"/var/lib/containers\"]:' ./config.toml
  podman secret create config.toml ./config.toml
  rm config.toml
  podman container runlabel setupstorage \$PIPGLR_IMAGE
  podman container runlabel setupcache \$PIPGLR_IMAGE
  podman container runlabel run \$PIPGLR_IMAGE
  mkdir -p ~/.local/share/systemd/user
  podman generate systemd --name duniter-pipglr >~/.local/share/systemd/user/duniter-pipglr.service
  systemctl --user daemon-reload
  systemctl --user enable duniter-pipglr.service
  sleep 10
  podman stop duniter-pipglr
"

# Start the service
sudo loginctl disable-linger "$PIPGLR_USER"
sleep 5
sudo loginctl enable-linger "$PIPGLR_USER"
